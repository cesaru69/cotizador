﻿namespace CotizacionesFibraOpticaApi.Common.Settings
{
    public class AuthDto
    {
        public string username { get; set; }
        public string password { get; set; }
        public string tipoaplicativo { get; set; }
    }
}
