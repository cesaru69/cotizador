﻿using CotizacionesFibraOpticaApi.Service.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CotizacionesFibraOpticaApi.Persistence
{
    public partial class WebApiDbContext : DbContext, IWebApiDbContext
    {
        public WebApiDbContext(DbContextOptions<WebApiDbContext> options)
            : base(options)
        {
        }
    }
}
