﻿IF (OBJECT_ID('[dbo].[CreateCustomerProc]') IS NOT NULL)  DROP PROCEDURE [dbo].[CreateCustomerProc]
GO
CREATE PROCEDURE [dbo].CreateCustomerProc
	@NameCustomer varchar(100),
	@Company varchar(80),
	@Phone varchar(15),
	@Email varchar(100),
	@Loevm bit,
	@NomOun varchar(100),
	@NomUsu varchar(100),
	@UsuAct varchar(100)
AS
BEGIN
	SET NOCOUNT ON
		INSERT INTO Customer (
			NameCustomer,
			Company,
			Phone,
			Email,
			Loevm,
			NomOun,
			NomUsu,
			UsuAct,
			FechMov,
			FechAct
		)
		values (
			@NameCustomer,
			@Company,
			@Phone,
			@Email,
			@Loevm,
			@NomOun,
			@NomUsu,
			@UsuAct,
			GETDATE(),
			GETDATE()
		)
	SET NOCOUNT OFF
END