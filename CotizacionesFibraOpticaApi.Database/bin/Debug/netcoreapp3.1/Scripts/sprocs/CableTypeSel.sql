﻿IF (OBJECT_ID('[dbo].[CableTypeSel]') IS NOT NULL)  DROP PROCEDURE [dbo].[CableTypeSel]
GO
CREATE PROCEDURE [dbo].CableTypeSel
 @Id int = null
AS
BEGIN
    SET NOCOUNT ON

		SELECT * FROM CableType WHERE id = ISNULL(@Id, id)

  SET NOCOUNT OFF
END