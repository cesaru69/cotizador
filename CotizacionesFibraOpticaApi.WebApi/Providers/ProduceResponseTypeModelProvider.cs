﻿using CotizacionesFibraOpticaApi.WebApi.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace CotizacionesFibraOpticaApi.WebApi.Providers
{
    public class ProduceResponseTypeModelProvider : IApplicationModelProvider
    {
        public int Order => 3;

        public void OnProvidersExecuted(ApplicationModelProviderContext context)
        {
        }

        public void OnProvidersExecuting(ApplicationModelProviderContext context)
        {
            foreach (ControllerModel controller in context.Result.Controllers)
            {
                foreach (ActionModel action in controller.Actions)
                {
                    var returnTypes = action.ActionMethod.ReturnType.GenericTypeArguments[0].GetGenericArguments();

                    if (returnTypes.Length > 0)
                    {
                        var returnType = returnTypes[0];

                        action.Filters.Add(new ProducesResponseTypeAttribute(returnType, StatusCodes.Status200OK));
                        action.Filters.Add(new ProducesResponseTypeAttribute(typeof(ApiErrorResult), StatusCodes.Status400BadRequest));
                        action.Filters.Add(new ProducesResponseTypeAttribute(typeof(ApiErrorResult), StatusCodes.Status500InternalServerError));
                    }
                }
            }
        }
    }
}
