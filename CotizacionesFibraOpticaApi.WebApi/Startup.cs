using CotizacionesFibraOpticaApi.Persistence;
using CotizacionesFibraOpticaApi.Service.Interfaces;
using CotizacionesFibraOpticaApi.WebApi.Extensions;
using CotizacionesFibraOpticaApi.WebApi.Filters;
using CotizacionesFibraOpticaApi.WebApi.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CotizacionesFibraOpticaApi.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(WebApiExceptionFilter));
                options.Filters.Add(typeof(ModelValidationFilter));
            }).AddNewtonsoftJson();

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            services.TryAddEnumerable(ServiceDescriptor.Transient<IApplicationModelProvider, ProduceResponseTypeModelProvider>());

            services.AddDbContextPool<IWebApiDbContext, WebApiDbContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase")));

            services.AddTransientServices();

            services.AddWebApiCorsPolicy();

            services.AddOptions();

            services.AddJwtAuthentication(Configuration["JwtAuthentication:Secret"]);

            services.AddWebApiOptions(Configuration);

            services.AddSwaggerDocumentation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
