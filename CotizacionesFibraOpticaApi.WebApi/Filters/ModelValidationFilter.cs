﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net;

namespace CotizacionesFibraOpticaApi.WebApi.Filters
{
    public sealed class ModelValidationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {
                var validationErrors = actionContext.ModelState.Values
                                                                .SelectMany(err => err.Errors)
                                                                .Select(err => err.ErrorMessage);

                actionContext.HttpContext.Response.ContentType = "application/json";
                actionContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                actionContext.Result = new JsonResult(new ApiErrorResult
                {
                    Message = "Error de Validación",
                    TipoError = TipoError.Validacion,
                    ValidationErrors = validationErrors
                });

                base.OnActionExecuting(actionContext);
            }
        }
    }
}
