﻿using CotizacionesFibraOpticaApi.Service.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;

namespace CotizacionesFibraOpticaApi.WebApi.Filters
{
    public class WebApiExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<WebApiExceptionFilter> _logger;
        private readonly IHostEnvironment _hostEnvironment;

        public WebApiExceptionFilter(ILogger<WebApiExceptionFilter> logger, IHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
            _logger = logger;
        }


        public void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            var message = context.Exception.Message;

            if (context.Exception is SqlException || context.Exception is WebApiException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(new ApiErrorResult
                {
                    Message = message,
                    TipoError = TipoError.Negocio
                });

                return;
            }

            var isDevelopment = _hostEnvironment.IsDevelopment();

            if (isDevelopment)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(new ApiErrorResult
                {
                    Message = message,
                    TipoError = TipoError.Negocio
                });
            }
            else
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Result = new JsonResult(new ApiErrorResult
                {
                    Message = message,
                    StackTrace = context.Exception.StackTrace,
                    TipoError = TipoError.Negocio
                });
            }
        }
    }


    public class ApiErrorResult
    {
        public string Message { get; set; }
        public IEnumerable<string> ValidationErrors { get; set; }
        public string StackTrace { get; set; }
        public TipoError TipoError { get; set; }
    }

    public enum TipoError
    {
        Validacion = 1,
        Negocio = 2
    }
}
