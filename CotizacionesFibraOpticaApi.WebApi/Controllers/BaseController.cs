﻿using Microsoft.AspNetCore.Mvc;

namespace CotizacionesFibraOpticaApi.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public abstract class BaseController : ControllerBase
    {
    }
}
