﻿using CotizacionesFibraOpticaApi.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.WebApi.Controllers
{
    //[Authorize]
    [ApiExplorerSettings(GroupName = "General")]
    public class ExecuteController : BaseController
    {
        private readonly IExecuteService _executeService;
        public ExecuteController(IExecuteService executeService)
        {
            _executeService = executeService;
        }

        [HttpPost]
        public async Task<ActionResult<IList<dynamic>>> GetList(string storedProcedure, string parameters = null)
        {
            var parsedParameters = parameters == null ? null : JsonConvert.DeserializeObject<JObject>(parameters);

            var dynamicListObject = await _executeService.GetList(storedProcedure, parsedParameters);

            return Ok(dynamicListObject);
        }


        [HttpGet]
        public async Task<ActionResult<dynamic>> Get(string storedProcedure, string parameters)
        {
            var parsedParameters = parameters == null ? null : JsonConvert.DeserializeObject<JObject>(parameters);

            var dynamicObject = await _executeService.Get(storedProcedure, parsedParameters);

            return Ok(dynamicObject);
        }
    }
}
