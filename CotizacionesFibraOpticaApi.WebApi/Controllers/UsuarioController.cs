﻿using CotizacionesFibraOpticaApi.Common.Settings;
using CotizacionesFibraOpticaApi.Infraestructure;
using CotizacionesFibraOpticaApi.Service.Customer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.WebApi.Controllers
{
    [ApiExplorerSettings(GroupName = "General")]
    public class UsuarioController : BaseController
    {
        private readonly ICustomerService _customerService;
        private readonly IHttpAuth _httpAuth;

        public UsuarioController(ICustomerService customerService, IHttpAuth httpAuth)
        {
            _customerService = customerService;
            _httpAuth = httpAuth;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Create([FromBody] CreateCustomerDto customerDto)
        {
            await _customerService.CreateCustomer(customerDto);

            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<string>> Auth([FromBody] AuthDto authDto)
        {
            return await _httpAuth.Auth(authDto);
        }
    }
}
