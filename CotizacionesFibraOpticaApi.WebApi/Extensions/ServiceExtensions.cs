﻿using CotizacionesFibraOpticaApi.Common.Settings;
using CotizacionesFibraOpticaApi.Infraestructure;
using CotizacionesFibraOpticaApi.Service;
using CotizacionesFibraOpticaApi.Service.Customer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.AspNetCore;
using NSwag.Generation.Processors.Security;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddTransientServices(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUserAccessor, UserAccessor>();
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IExecuteService, ExecuteService>();
            services.AddTransient<IDatabaseScriptUpdater, DatabaseScriptUpdater>();
            services.AddTransient<IHttpAuth, HttpAuth>();
        }


        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document = GetSwaggerCommonSettings(document);

                document.DocumentName = "General";
                document.Description = "APIS exclusivas de General";
                document.ApiGroupNames = new[] { "General" };
            });

            return services;
        }

        private static AspNetCoreOpenApiDocumentGeneratorSettings GetSwaggerCommonSettings(AspNetCoreOpenApiDocumentGeneratorSettings document)
        {
            document.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
            {
                Type = OpenApiSecuritySchemeType.ApiKey,
                Name = "Authorization",
                In = OpenApiSecurityApiKeyLocation.Header,
                Description = "Type into the textbox: Bearer {your JWT token}."
            });

            document.OperationProcessors.Add(
                new AspNetCoreOperationSecurityScopeProcessor("JWT"));

            document.Title = "WebApi";

            return document;
        }

        public static void AddConfigOptions<TOptions>(this IServiceCollection services, IConfiguration configuration, string section) where TOptions : class, new()
        {
            IConfigurationSection sec = configuration.GetSection(section);
            services.Configure<TOptions>(sec);
        }

        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, string jwtSecretKey)
        {
            var securityKey = Encoding.ASCII.GetBytes(jwtSecretKey);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(securityKey),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = false
                    };

                    x.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                            {
                                context.Response.Headers.Add("Token-Expired", "true");
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            return services;
        }

        public static IServiceCollection AddWebApiOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();
            services.AddConfigOptions<JwtAuthentication>(configuration, "JwtAuthentication");

            return services;
        }


        public static IServiceCollection AddWebApiCorsPolicy(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                     builder => builder.WithOrigins("http://localhost:4200")
                     .AllowAnyMethod()
                     .AllowAnyHeader()
                     .AllowCredentials());
            });

            return services;
        }
    }
}
