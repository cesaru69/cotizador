﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using roundhouse;
using System.IO;

namespace CotizacionesFibraOpticaApi.Infraestructure
{
    public class DatabaseScriptUpdater : IDatabaseScriptUpdater
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _hostEnvironment;

        public DatabaseScriptUpdater(IConfiguration configuration, IHostEnvironment hostEnvironment)
        {
            _configuration = configuration;
            _hostEnvironment = hostEnvironment;
        }


        public void RunScripts()
        {
            var scriptsDirectory = _hostEnvironment.IsProduction()
                ? "./bin/Release/netcoreapp3.1/Scripts"
                : "./bin/Debug/netcoreapp3.1/Scripts";


            var migrator = new Migrate().Set(c =>
            {
                c.ConnectionString = _configuration.GetConnectionString("WebApiDatabase");
                c.CommandTimeout = 500;
                c.SqlFilesDirectory = scriptsDirectory;
                c.WithTransaction = false;
                c.DisableOutput = true;
                c.OutputPath = Path.GetTempPath();
                c.Silent = true;
            });

            migrator.Run();
        }
    }

    public interface IDatabaseScriptUpdater
    {
        void RunScripts();
    }
}
