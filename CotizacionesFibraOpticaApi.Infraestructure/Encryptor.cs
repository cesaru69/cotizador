﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CotizacionesFibraOpticaApi.Infraestructure
{
    public class Encryptor : IEncryptor
    {
        private static Encryptor _instance;
        private const string KeyForEncryptor = "abcdefgh";
        private const string InitializationVectorForEncryptor = "hgfedcba";

        public Encryptor() { }

        public static Encryptor GetInstance()
        {
            return _instance ?? (_instance = new Encryptor());
        }

        public byte[] Encrypt(byte[] bytesData)
        {
            try
            {
                var key = Encoding.ASCII.GetBytes(KeyForEncryptor);
                var iv = Encoding.ASCII.GetBytes(InitializationVectorForEncryptor);

                DES des = new DESCryptoServiceProvider();
                des.Mode = CipherMode.CBC;
                des.Key = key;
                des.IV = iv;

                var transform = des.CreateEncryptor();

                //Set up the stream that will hold the encrypted data.
                var memStreamEncryptedData = new MemoryStream();
                CryptoStream encStream = new CryptoStream(memStreamEncryptedData, transform, CryptoStreamMode.Write);

                //Encrypt the data, write it to the memory stream.
                encStream.Write(bytesData, 0, bytesData.Length);

                //Clean Stream
                encStream.FlushFinalBlock();
                encStream.Close();

                //Send the data back.
                var cipherText = memStreamEncryptedData.ToArray();
                memStreamEncryptedData.Close();
                return cipherText;

            }
            catch (CryptographicException ex)
            {
                System.Diagnostics.Trace.WriteLine("Encryptor Encrypt -- " + ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Encryptor Encrypt -- " + ex.Message);
                throw;
            }

        }

        public byte[] Decrypt(byte[] bytesData)
        {
            try
            {
                var key = Encoding.ASCII.GetBytes(KeyForEncryptor);
                var iv = Encoding.ASCII.GetBytes(InitializationVectorForEncryptor);
                byte[] encrypted;

                DES des = new DESCryptoServiceProvider();
                des.Mode = CipherMode.CBC;
                des.Key = key;
                des.IV = iv;

                var transform = des.CreateDecryptor();

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, transform, CryptoStreamMode.Write))
                    {
                        csEncrypt.Write(bytesData, 0, bytesData.Length);

                        //This was not closing the cryptostream and only worked if I called FlushFinalBlock()
                        //encrypted = msEncrypt.ToArray(); 
                    }

                    encrypted = msEncrypt.ToArray();

                    return encrypted;
                }

                //Set up the memory stream for the decrypted data.
                var memStreamDecryptedData = new MemoryStream();
                CryptoStream decStream = new CryptoStream(memStreamDecryptedData, transform, CryptoStreamMode.Write);

                //Decrypt the data, write it to the memory stream.
                decStream.Write(bytesData, 0, bytesData.Length);

                var cipherText = memStreamDecryptedData.ToArray();

                //Clean Stream
                //decStream.FlushFinalBlock();
                decStream.Close();

                //Send the data back.
                memStreamDecryptedData.Close();
                return cipherText;

            }
            catch (CryptographicException ex)
            {
                System.Diagnostics.Trace.WriteLine("Encryptor Decrypt -- " + ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Encryptor Decrypt -- " + ex.Message);
                throw;
            }
        }
    }

    public interface IEncryptor
    {
        byte[] Decrypt(byte[] bytesData);
        byte[] Encrypt(byte[] bytesData);
    }
}
