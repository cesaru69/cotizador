﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace CotizacionesFibraOpticaApi.Infraestructure
{
    public class UserAccessor : IUserAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ClaimsPrincipal User => _httpContextAccessor.HttpContext.User;

        public UserAccessor(IHttpContextAccessor accessor)
        {
            _httpContextAccessor = accessor ?? throw new ArgumentNullException(nameof(accessor));
        }

        public string GetNombrePcMod()
        {
            return _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }
        public string GetUserLoginUserName()
        {
            return ReturnClaimOrEmptyString("LoginUserName");
        }

        public int GetUserClaSucursal()
        {
            return Convert.ToInt32(ReturnClaimOrEmptyString("ClaUbicacion"));
        }

        public int GetUserId()
        {
            return Convert.ToInt32(ReturnClaimOrEmptyString("IdUsuario"));
        }

        public int GetUserRolId()
        {
            return Convert.ToInt32(ReturnClaimOrEmptyString("IdRol"));
        }

        public string GetUserNombreCompleto()
        {
            return ReturnClaimOrEmptyString("NombreCompleto");
        }

        public string GetUserEmail()
        {
            return ReturnClaimOrEmptyString("Email");
        }

        private string ReturnClaimOrEmptyString(string claimName)
        {
            var claim = User.FindFirst(claimName);

            return claim != null ? claim.Value : string.Empty;
        }
    }

    public interface IUserAccessor
    {
        string GetNombrePcMod();
        string GetUserLoginUserName();
        int GetUserClaSucursal();
        int GetUserId();
        int GetUserRolId();
        string GetUserNombreCompleto();
        string GetUserEmail();
    }
}
