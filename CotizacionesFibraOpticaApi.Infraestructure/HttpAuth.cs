﻿using CotizacionesFibraOpticaApi.Common.Settings;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.Infraestructure
{
    public class HttpAuth : IHttpAuth
    {
        public async Task<string> Auth(AuthDto authDto)
        { 
            var result = "";

                WebRequest oRequest = WebRequest.Create("http://fyi.steren.com.mx/SterenServicesTest/api/servicio/autenticar");
                oRequest.Method = "post";
                oRequest.ContentType = "application/json";

            using (var oSW = new StreamWriter(oRequest.GetRequestStream()))
                {
                    oSW.Write(JsonConvert.SerializeObject(authDto));
                    oSW.Flush();
                    oSW.Close();
                }

                WebResponse oResponse = oRequest.GetResponse();
                            using (var oSR = new StreamReader(oResponse.GetResponseStream()))
                            {
                                var result2 = oSR.ReadToEndAsync().Result;
                result = JsonConvert.DeserializeObject<string>(result2);
                    }


            return result;
        }
    }
    public interface IHttpAuth
    {
        Task<string> Auth(AuthDto authDto);
    }
}
