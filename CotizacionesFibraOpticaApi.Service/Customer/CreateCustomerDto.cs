﻿using System;

namespace CotizacionesFibraOpticaApi.Service.Customer
{
    public class CreateCustomerDto
    {
        public string NameCustomer { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Loevm { get; set; }
        public string NomOun { get; set; }
        public string NomUsu { get; set; }
        public string UsuAct { get; set; }
        public DateTime FechMov { get; set; }
        public DateTime FechAct { get; set; }
    }
}
