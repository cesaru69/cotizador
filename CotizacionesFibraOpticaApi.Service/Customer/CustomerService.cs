﻿using CotizacionesFibraOpticaApi.Common.Constants;
using CotizacionesFibraOpticaApi.Service.Interfaces;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.Service.Customer
{
    public class CustomerService : ICustomerService
    {
        private readonly IWebApiDbContext _webApiDbContext;
        public CustomerService(IWebApiDbContext webApiDbContext)
        {
            _webApiDbContext = webApiDbContext;
        }
        public async Task CreateCustomer(CreateCustomerDto customerDto)
        {
            await _webApiDbContext.CallStoredProcedure(SpConstants.CreateCustomerProc)
                                        .AddParameter("@NameCustomer", customerDto.NameCustomer)
                                        .AddParameter("@Company", customerDto.Company)
                                        .AddParameter("@Phone", customerDto.Phone)
                                        .AddParameter("@Email", customerDto.Email)
                                        .AddParameter("@Loevm", customerDto.Loevm)
                                        .AddParameter("@NomOun", customerDto.NomOun)
                                        .AddParameter("@NomUsu", customerDto.NomUsu)
                                        .AddParameter("@UsuAct", customerDto.UsuAct)
                                        .ExecuteNonQuery();
        }
    }
    public interface ICustomerService
    {
        Task CreateCustomer(CreateCustomerDto customerDto);
    }
}
