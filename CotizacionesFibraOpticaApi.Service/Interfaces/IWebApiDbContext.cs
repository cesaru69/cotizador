﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.Service.Interfaces
{
    public interface IWebApiDbContext
    {
        IWebApiDbContext CallStoredProcedure(string storedProcedureName);
        IWebApiDbContext AddParameter(string name, object value);
        IWebApiDbContext AddParameters(Dictionary<string, object> parameters = null);
        Task<T> Execute<T>() where T : new();
        Task ExecuteNonQuery();
    }
}
