﻿using System;
using System.Runtime.Serialization;

namespace CotizacionesFibraOpticaApi.Service.Exceptions
{
    [Serializable]
    public class WebApiException : Exception
    {
        public int StatusCode { get; set; }


        public WebApiException(string message,
                            int statusCode = 500) :
            base(message)
        {
            StatusCode = statusCode;
        }


        public WebApiException(Exception ex, int statusCode = 500) : base(ex.Message)
        {
            StatusCode = statusCode;
        }

        protected WebApiException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
