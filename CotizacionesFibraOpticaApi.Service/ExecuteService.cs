﻿using CotizacionesFibraOpticaApi.Service.Interfaces;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CotizacionesFibraOpticaApi.Service
{
    public class ExecuteService : IExecuteService
    {
        private readonly IWebApiDbContext _webApiDbContext;
        public ExecuteService(IWebApiDbContext webApiDbContext)
        {
            _webApiDbContext = webApiDbContext;
        }


        public async Task<IList<dynamic>> GetList(string storedProcedureName, JObject parameters)
        {
            var parsedParameters = parameters?.ToObject<Dictionary<string, object>>();

            var dynamicObject = await _webApiDbContext
                                    .CallStoredProcedure(storedProcedureName)
                                    .AddParameters(parsedParameters)
                                    .Execute<List<dynamic>>();

            return dynamicObject;
        }

        public async Task<dynamic> Get(string storedProcedureName, JObject parameters)
        {
            var parsedParameters = parameters?.ToObject<Dictionary<string, object>>();

            var dynamicObject = await _webApiDbContext
                                    .CallStoredProcedure(storedProcedureName)
                                    .AddParameters(parsedParameters)
                                    .Execute<dynamic>();

            return dynamicObject;
        }
    }

    public interface IExecuteService
    {
        Task<IList<dynamic>> GetList(string storedProcedureName, JObject parameters);

        Task<dynamic> Get(string storedProcedureName, JObject parameters);
    }
}
